//直接import graphql server class，instance之後直接起動。
//typeDefs,resolvers<-- 會是下一篇文章詳情講解
import {ApolloServer,gql} from 'apollo-server-express';
import Express from 'express';

class Person{
    public name:string;
    public age: number;
    public email: string;
    constructor(name:string,age:number,email:string){
        this.name = name;
        this.age = age;
        this.email = email;
    }
}

const personList:Person[] = [];
personList.push(new Person("Tom",34,"tom@gmail.com"));
personList.push(new Person("Peter",30,"peter@gmail.com"));
personList.push(new Person("Ken",25,"ken@gmail.com"));

const app = Express();
const typeDefs = gql`
    type Person{
        name:String,
        age:Int,
        email:String
    }
    type Query{
        person:[Person]
    }
`;
const resolvers= {
    Query:{person:()=>personList},
    Person:{
        name:(parent:Person)=>parent.name,
        age:(parent:Person)=>parent.age,
        email:(parent:Person)=>parent.email
    }
};

const server = new ApolloServer({typeDefs,resolvers});
server.applyMiddleware({app,path:'/graphql'}); //<-- 記得是"GraphQL概念入門"開頭提過Middileware既概念嗎？
app.listen(4000,()=>{
    console.log("GraphQL Server is started at http://localhost:4000/graphql");
});